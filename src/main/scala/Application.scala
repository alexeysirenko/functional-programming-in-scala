import fpinscala.excercise2_1.FibonacciTailRecursive
import fpinscala.excercise3._
import fpinscala.exercise4.TryHelper
import fpinscala.exercise4._
import fpinscala.laziness._
import fpinscala.state._

/**
  * Created by sirenko on 24.02.17.
  */
object Application extends App {

  val x = 6
  // Excercise 2.1. Calculate a Fibonacci number
  println(s"Fibonacci of $x is ${FibonacciTailRecursive.fib(x)}")

  val testList = List("foo", "bar", "baz", "quok", "eggs", "spam")

  println(CustomList.tail(testList))

  println(CustomList.setHead("quuuk", testList))

  println(CustomList.drop( testList, 4))

  println(CustomList.dropWhile( testList) (x => x.length < 4))

  println(CustomList.length3( testList))

  println(CustomList.reverse(testList))

  println(CustomList.concat(List(testList, testList)))

  println(CustomList.addOne(List(1, 2, 3)))

  println(CustomList.toStr(List(1, 2, 3)))

  println(CustomList.map(testList)(_ + "s"))

  println(CustomList.filter(List(1, 2, 3, 4, 5))((x) => x % 2 == 0))

  println(CustomList.flatMap(List(1, 2, 3, 4, 5))((x) => List(x, x)))

  println(CustomList.flatMap(testList)((x) => if (x.length > 3) List(x) else Nil))

  println(CustomList.addPairwise(List(1, 2, 3), List(1, 2, 3)))

  println(CustomList.zipWith(List(1, 2, 3), List(1, 2, 3))(_ * _))

  println(CustomList.hasSubsequence(List(1, 2, 3, 4, 5), List(3, 4, 5)))

  println(CustomTrees.size(Branch(Leaf("foo"), Branch(Leaf("bar"), Leaf("baz")))))

  println(CustomTrees.maximum(Branch(Leaf(1), Leaf(2))))

  println(CustomTrees.depth(Branch(Branch(Leaf(1), Leaf(2)), Leaf(3))))

  println(CustomTrees.map(Branch(Branch(Leaf(1), Leaf(2)), Leaf(3)))(_ * 5))

  println(Option.sequence(List(Some("foo"))))

  println(TryHelper.sequence(List()))

  println(Stream(1, 2).toList)

  println(Stream(1, 2, 3, 4, 5, 6, 7).drop(1).take(5).takeWhile(it => it < 5).toList) //

  println(RNG.nonNegativeInt(SimpleRNG(42)))

  println(RNG.ints(3)(SimpleRNG(42)))


  println(RNG.ints(3)(SimpleRNG(42)))

}
