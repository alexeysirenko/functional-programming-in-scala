package fpinscala.excercise2_2

import scala.collection.LinearSeq
import scala.annotation.tailrec

/**
  * Created by sirenko on 01.03.17.
  */
class SortedExample {

  def isSorted[A](as: LinearSeq[A], isOrdered: (A, A) => Boolean): Boolean = {

    @tailrec
    def isSortedHelper(items: LinearSeq[A], cond: (A, A) => Boolean): Boolean = {
      items match {
        case Seq() => true
        case Seq(_) => true
        case head :: tail => cond(head, tail.head) && isSortedHelper(tail, cond)
      }
    }

    isSortedHelper(as, isOrdered)
  }

}
