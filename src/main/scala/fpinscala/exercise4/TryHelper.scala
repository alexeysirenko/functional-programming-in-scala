package fpinscala.exercise4

import scala.annotation.tailrec
import scala.util.{Failure, Success, Try}

/**
  * Created by sirenko on 13.03.17.
  */
object TryHelper {

  def sequence[A](in: List[Try[A]]): Try[List[A]] = {

    @tailrec
    def sequenceRecursive[B](in: List[Try[B]])(accumulator: List[B]): Try[List[B]] = in match {
      case Nil => Success(accumulator)
      case Failure(head) :: _ => Failure(head)
      case Success(head) :: maybeTail => sequenceRecursive(maybeTail)(accumulator :+ head)
    }

    sequenceRecursive(in)(List.empty)
  }

  implicit def fromTry2TryHelper(obj: Try.type): TryHelper.type = this
}
