package fpinscala.excercise3

import scala.annotation.tailrec

object CustomList {

  def tail[A](xs: List[A]): List[A] = xs match {
    case Nil => throw new UnsupportedOperationException("Empty list does not have a tail")
    case _ :: tail => tail
  }

  def setHead[A](head: A, xs: List[A]): List[A] = xs match {
    case Nil => head :: Nil
    case _ :: tail => head :: tail
  }

  def drop[A](xs: List[A], firstN: Int): List[A] = {
    if (firstN == 0) xs
    else xs match {
      case Nil => Nil
      case _ :: tail => drop(tail, firstN - 1)
    }
  }

  @tailrec
  def dropWhile[A](l: List[A])(f: A => Boolean): List[A] = {
    l match {
     case head :: tail if f(head) => dropWhile(tail)(f)
     case _ => l
    }
  }


  def foldRight[A,B](as: List[A], z: B)(f: (A, B) => B): B =
    as match {
      case Nil => z
      case x :: xs => f(x, foldRight(xs, z)(f))
    }

  def sum2(ns: List[Int]) =
    foldRight(ns, 0)((x,y) =>x+y)

  def product2(ns: List[Double]) =
    foldRight(ns, 1.0)(_ * _)

  def length2[A](as: List[A]): Int =
    foldRight(as, 0)((_ ,y) => y + 1)

  @tailrec
  def foldLeft[A,B](as: List[A], z: B)(f: (B, A) => B): B = as match {
    case Nil => z
    case head :: tail => foldLeft(tail, f(z, head))(f)
  }

  def sum3(ns: List[Int]) =
    foldLeft(ns, 0)((x,y) =>x+y)

  def product3(ns: List[Double]) =
    foldLeft(ns, 1.0)(_ * _)

  def length3[A](as: List[A]): Int =
    foldLeft(as, 0)((x ,_) => x + 1)

  def reverse[A](l: List[A]): List[A] = foldLeft(l, List.empty[A])((acc, head) => head :: acc)

  def append[A](f: List[A], s: List[A]): List[A] = f ++ s

  def concat[A](lists: List[List[A]]): List[A] = foldLeft(lists, List.empty[A])((acc, head) => acc ++ head)

  def addOne(l: List[Int]): List[Int] = foldLeft(l, List.empty[Int])((acc, head) => acc :+ (head + 1))

  def toStr(l: List[Double]): List[String] = foldLeft(l, List.empty[String])((acc, head) => acc :+ head.toString)

  def map[A, B](l: List[A])(f: A => B): List[B] = foldLeft(l, List.empty[B])((acc, head) => acc :+ f(head))

  def filter[A](as: List[A])(f: A => Boolean): List[A] = foldLeft(as, List.empty[A])((acc, head) => if (f(head)) acc :+ head else acc)

  def flatMap[A,B](as: List[A])(f: A => List[B]): List[B] = foldLeft(as, List.empty[B])((acc, head) => acc ++ f(head))

  def addPairwise(a: List[Int], b: List[Int]): List[Int] = (a, b) match {
    case (_, Nil) => Nil
    case (Nil, _) => Nil
    case (head1 :: tail1, head2 :: tail2) => (head1 + head2) :: addPairwise(tail1, tail2)
  }

  def zipWith[A, B, C](a: List[A], b: List[B])(f: (A, B) => C): List[C] = (a, b) match {
    case (_, Nil) => Nil
    case (Nil, _) => Nil
    case (head1 :: tail1, head2 :: tail2) => f(head1, head2) :: zipWith(tail1, tail2)(f)
  }

  @tailrec
  def hasSubsequence[A](sup: List[A], sub: List[A]): Boolean = sup match {
    case Nil => sub == Nil
    case l if l.startsWith(sub) => true
    case head :: tail => hasSubsequence(tail, sub)
  }

}
