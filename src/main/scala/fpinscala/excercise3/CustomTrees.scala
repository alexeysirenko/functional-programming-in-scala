package fpinscala.excercise3

/**
  * Created by Oleksiy on 10.03.2017.
  */

sealed trait Tree[+A]
case class Leaf[A](value: A) extends Tree[A]
case class Branch[A](left: Tree[A], right: Tree[A]) extends Tree[A]


object CustomTrees {

  def size[A](tree: Tree[A]): Int = tree match {
    case Branch(left, right) => 1 + size(left) + size(right)
    case Leaf(_) => 1
  }

  def maximum(tree: Tree[Int]): Int = tree match {
    case Branch(left, right) => maximum(left) max maximum(right)
    case Leaf(value) => value
  }

  def depth[A](t: Tree[A]): Int = t match {
    case Leaf(_) => 0
    case Branch(left, right) => 1 + (depth(left) max depth(right))
  }

  def map[A, B](t: Tree[A])(f: A => B): Tree[B] = t match {
    case Leaf(value) => Leaf(f(value))
    case Branch(left, right) => Branch(map(left)(f), map(right)(f))
  }

  def fold[A, B](l: Tree[A])(f: A => B)(g: (B, B) => B): B = l match {
    case Leaf(value) => f(value)
    case Branch(left, right) => g(fold(left)(f)(g), fold(right)(f)(g))
  }

  def sizeViaFold[A](l: Tree[A]): Int = fold(l)(_ => 1)(1 + _ + _)

  def maxViaFold(l: Tree[Int]): Int = fold(l)(a => a)(_ max _)

  def depthViaFold[A](l: Tree[A]): Int = fold(l)(_ => 0)((d1, d2) => 1 + (d1 max d2))

  def mapViaFold[A, B](l: Tree[A])(f: A => B): Tree[B] = fold(l)(a => Leaf(f(a)): Tree[B])(Branch(_,_))
}
