package fpinscala.excercise2_1

import scala.annotation.tailrec

/**
  * Created by sirenko on 24.02.17.
  */
object FibonacciTailRecursive {

  def fib(x: Int): BigInt = {
    @tailrec def fibHelper(x: Int, prev: BigInt = 0, next: BigInt = 1): BigInt = x match {
      case 0 => prev
      case 1 => next
      case _ => fibHelper(x - 1, next, next + prev)
    }
    fibHelper(x)
  }

}
