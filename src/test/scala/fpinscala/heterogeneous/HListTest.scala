package fpinscala.heterogeneous

import fpinscala.heterogeneous.HList.HNil
import org.scalatest.FunSuite

class HListTest extends FunSuite {

  test("should concatenate and parse heterogeneous list") {
    val x = 2 :: false :: 2 :: Some(4) :: "foo" :: HNil
    val a = x.head // Int
    val b = x.tail.head // Boolean

    assert(a == 2)
    assert(b == false)
  }

}
