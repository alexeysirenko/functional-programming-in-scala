package fpinscala.monads

import org.scalatest.FunSuite

class MonadTest extends FunSuite {

  import Monad._

  test("testReplicateM") {

    val l = listMonad.unit(5)
    assert(listMonad.replicateM(3, l) == List(List(5, 5, 5)))

    val o = optionMonad.unit(5)
    assert(optionMonad.replicateM(3, o) == Some(List(5, 5, 5)))
  }

}
